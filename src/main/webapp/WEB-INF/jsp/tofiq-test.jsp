<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 04.11.19
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Tofiq Test</title>
</head>
<body>
    <h1>Tofiq kisi isleyir :) </h1>

    <h3>Formu doldurun!</h3>

    <c:if test="${not empty ad}">
        <p>
            Ad: ${ad} <br/>
            Soyad: ${soyad} <br/>
            Maas: ${maas} <br/>
        </p>
    </c:if>

    <form action="test" method="post">
        Ad: <input type="text" name="ad" /> <br/>
        Soyad: <input type="text" name="soyad"/> <br/>
        Maas: <input type="text" name="maas"/> <br/>
        <input type="submit" value="Gonder"/>
    </form>
</body>
</html>
