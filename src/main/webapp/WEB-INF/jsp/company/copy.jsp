
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
        <div class="jp_rightside_job_categories_heading">
            <h4>Company Overview</h4>
        </div>
        <div class="jp_jop_overview_img_wrapper">
            <div class="jp_jop_overview_img">
                <img src="images/content/web.png" alt="post_img" />
            </div>
        </div>
        <div class="jp_job_listing_single_post_right_cont">
            <div class="jp_job_listing_single_post_right_cont_wrapper">
                <h4>${company.name}</h4>
                <p>${company.about}</p>
            </div>
        </div>
        <div class="jp_job_post_right_overview_btn_wrapper">
            <div class="jp_job_post_right_overview_btn">
                <ul>
                    <%--                                        <li><a href="#">Bazandan Gelecey</a></li>--%>
                </ul>
            </div>
        </div>
        <div class="jp_listing_overview_list_outside_main_wrapper">
            <div class="jp_listing_overview_list_main_wrapper">
                <div class="jp_listing_list_icon">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="jp_listing_list_icon_cont_wrapper">
                    <ul>
                        <li>Location</li>
                        <li>${company.headOffice}</li>
                    </ul>
                </div>
            </div>
            <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                <div class="jp_listing_list_icon">
                    <i class="fa fa-info-circle"></i>
                </div>
                <div class="jp_listing_list_icon_cont_wrapper">
                    <ul>
                        <li>Connection with ${company.name}</li>
                        <li>Company Email - ${company.email}</li>
                        <li>Company Phone - ${company.phone}</li>
                        <li>Company Mobile - ${company.mobile}</li>
                    </ul>
                </div>
            </div>
            <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                <div class="jp_listing_list_icon">
                    <i class="fa fa-th-large"></i>
                </div>
                <div class="jp_listing_list_icon_cont_wrapper">
                    <ul>
                        <li>Number Of Employees</li>
                        <li>${company.numberOfEmployees}</li>
                    </ul>
                </div>
            </div>
            <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                <div class="jp_listing_list_icon">
                    <i class="fa fa-star"></i>
                </div>
                <div class="jp_listing_list_icon_cont_wrapper">
                    <ul>
                        <li>Annual Revenue</li>
                        <li>${company.annualRevenue}</li>
                    </ul>
                </div>
                <div class="jp_listing_list_icon_cont_wrapper">
                    <ul>
                        <li>Company Type</li>
                        <li><c:choose>
                            <c:when test="${company.global}">
                                Global
                            </c:when>
                            <c:otherwise>Local</c:otherwise>
                        </c:choose></li>
                    </ul>
                </div>
                <div class="jp_listing_list_icon_cont_wrapper">
                    <ul>
                        <li>Annual Creation Date</li>
                        <li>${company.createDate}</li>
                    </ul>
                </div>
            </div>
            <div class="jp_listing_right_bar_btn_wrapper">
                <div class="jp_listing_right_bar_btn">
                    <ul>
                        <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Follow Facebook</a></li>
                        <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Follow NOw!</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>