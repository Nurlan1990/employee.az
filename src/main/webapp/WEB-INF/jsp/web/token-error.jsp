<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 30.10.19
  Time: 20:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <c:if test="${not empty requestScope.errorMessage}">
        <h2 style="color: red">${requestScope.errorMessage}</h2>
    </c:if>
</body>
</html>
