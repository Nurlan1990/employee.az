<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
    <div class="pager_wrapper gc_blog_pagination">
        <ul class="pagination">
            <c:choose>

                <c:when test="${currentPage<=1}">
                    <li><a href="#">Prev</a></li>
                </c:when>
                    <c:when test="${currentPage<=pageCount}">
                        <li><a href="candidates?page=${currentPage-1}">Prev</a></li>
                    </c:when>
                <c:otherwise>
                    <li><a href="#">Prev</a></li>
                </c:otherwise>
            </c:choose>

            <c:forEach begin="1" end="${pageCount}" var="i">
                <c:choose>
                    <c:when test="${i == currentPage}">
                        <li class="active"><a href="candidates?page=${i}">${i}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="candidates?page=${i}">${i}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:choose>
                <c:when test="${currentPage<=1}">
                    <li><a href="candidates?page=2">Next</a></li>
                </c:when>
                <c:when test="${currentPage<pageCount}">
                    <li><a href="candidates?page=${currentPage+1}">Next</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="#">Next</a></li>
                </c:otherwise>
            </c:choose>

        </ul>
    </div>
</div>