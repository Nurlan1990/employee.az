<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
    <div class="pager_wrapper gc_blog_pagination">
        <ul class="pagination">
            <c:choose>
                <c:when test="${currentPage<=1}">
                    <li><a href="#">Priv</a></li>
                </c:when>

                <c:when test="${currentPage<=pageSize}">
                    <li><a href="jobs?page=${currentPage-1}&category=${categoryID}">Priv</a></li>
                </c:when>
            </c:choose>

            <c:choose>
                <c:when test="${categoryID == null}">
            <c:forEach begin="1" end="${pageSize}" var="i">
<c:choose>
    <c:when test="${i==currentPage}">
        <li class="active"><a href="jobs?page=${i}">${i}</a></li>
    </c:when>
    <c:otherwise>
        <li> <a href="jobs?page=${i}">${i}</a></li>
    </c:otherwise>

</c:choose>
            </c:forEach>
                </c:when>
                <c:otherwise>
                    <c:forEach begin="1" end="${pageSize}" var="i">
                        <c:choose>
                            <c:when test="${i==currentPage}">
                                <li class="active"><a href="jobs?page=${i}&category=${categoryID}">${i}</a></li>
                            </c:when>
                            <c:otherwise>
                                <li> <a href="jobs?page=${i}&category=${categoryID}">${i}</a></li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <c:choose>
            <c:when test="${currentPage<=1}">
            <li><a href="jobs?page=2&category=${categoryID}">Next</a></li>
            </c:when>
                <c:when test="${currentPage==pageSize}">
                    <li><a href="#">Next</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="jobs?page=${currentPage+1}&category=${categoryID}">Next</a> </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>