package az.employee.interceptor;

import az.employee.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

import static az.employee.domain.Role.*;
import static az.employee.domain.Role.ADMIN;
import static az.employee.util.SecurityUtility.hasRole;

//@Component
public class EmployeeSecurityInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        boolean success = false;

        if(request.getSession().getAttribute("user") != null) {
            User user = (User) request.getSession().getAttribute("user");
            System.out.println("user login olub, user = " + user);
            System.out.println("rollari = " + user.getRoleList());
            success = true;

            URI uri = new URI(request.getRequestURL().toString());
            System.out.println("uri = " + uri);
            String url = getNormalizedUrl(uri, request);
            System.out.println("normalized url = " + url);
            String requestUrl = request.getRequestURL().toString();
            System.out.println("requestUrl = " + requestUrl);
            System.out.println("user roles = " + user.getRoleList());

            if(requestUrl.startsWith(url + "admin") && hasRole(user, ADMIN)) {
                System.out.println("allow to admin");
                success = true;
            } else if(requestUrl.startsWith(url + "company")
                    && (hasRole(user, COMPANY) || hasRole(user, ADMIN))) {
                System.out.println("allow to company");
                success = true;
            } else if(requestUrl.startsWith(url + "candidate")
                    && (hasRole(user, CANDIDATE) || hasRole(user, ADMIN))) {
                System.out.println("allow to candidate");
                success = true;
            } else {
                System.out.println("redirect to access denied");
                response.sendRedirect(request.getContextPath() + "/access-denied");
            }

        } else {
            success = false;
            System.out.println("user login olmayib, login sehifesine redirect edirik");
            response.sendRedirect(request.getContextPath() + "/login");
        }

        return success;
    }

    private static String getNormalizedUrl(URI uri, HttpServletRequest request) {
        String url = "";
        if(uri.getPort() != -1) {
            url = uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort() + "/" + request.getContextPath();
        } else {
            url = uri.getScheme() + "://" + uri.getHost() + "/" + request.getContextPath();
        }
        return url;
    }
}
