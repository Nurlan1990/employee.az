package az.employee.rest;

import az.employee.domain.Job;
import az.employee.domain.PageRequest;
import az.employee.domain.PageResponse;
import az.employee.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = {"/rest/jobs", "/rest/jobs/"})
@RestController
public class JobRestController {

    @Autowired
    private JobService jobService;

    @GetMapping("/getJobList")
    public PageResponse<Job> getJobList(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "sort", required = false, defaultValue = "id") String sortColumn,
            @RequestParam(name = "dir", required = false, defaultValue = "asc") String sortDirection,
            @RequestParam(name = "filter", required = false, defaultValue = "") String filter
    ) {
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(page);
        pageRequest.setSize(size);
        pageRequest.setSortColumn(sortColumn);
        pageRequest.setSortDir(sortDirection);
        pageRequest.setFilter(filter);
        System.out.println("page request = " + pageRequest);

        return jobService.getJobList(pageRequest);
    }

    @GetMapping("/recent")
    public List<Job> getRecentJobList() {
        return jobService.getRecentJobs();
    }

    @GetMapping("/{jobId}")
    public Job getJobById(@PathVariable(name = "jobId") long jobId) {
        Optional<Job> optionalJob = jobService.getJobById(jobId);

        if(optionalJob.isPresent()) {
            return optionalJob.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Job with id %d not found", jobId));
        }

    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public Job addJob(@RequestBody Job job) {
        System.out.println("add new job job = " + job);
        return jobService.addJob(job);
    }

    @PutMapping("/{jobId}")
    public Job updateJob(@PathVariable(name = "jobId") long jobId,
                         @RequestBody Job job) {
        Optional<Job> optionalJob = jobService.getJobById(jobId);
        if(optionalJob.isPresent()) {
            System.out.println("job id " + jobId + " found, update");
            return jobService.updateJob(job);
        } else {
            System.out.println("job not found, add new job " + job);
            return jobService.addJob(job);
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{jobId}")
    public void deleteJobById(@PathVariable(name = "jobId") long jobId) {
        boolean result = jobService.deleteJobById(jobId);
        if(!result) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Job with id %d not found", jobId));
        }
    }



}
