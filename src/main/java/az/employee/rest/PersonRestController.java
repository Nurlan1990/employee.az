package az.employee.rest;

import az.employee.entity.NameCount;
import az.employee.entity.Person;
import az.employee.repository.jpa.PersonRepository;
import az.employee.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/persons/")
public class PersonRestController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @GetMapping("/")
    public List<Person> getPersonList() {
        List<Person> personList = new ArrayList<>();
        personRepository.findAll().forEach(personList::add);
        return personList;
    }

    @GetMapping("/page")
    public Page<Person> getPersonPage(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "5") int size,
            @RequestParam(name = "sort", required = false, defaultValue = "person_id    ") String sortColumn,
            @RequestParam(name = "dir", required = false, defaultValue = "asc") String sortDirection,
            @RequestParam(name = "filter", required = false, defaultValue = "") String filter

    ) {

        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.fromString(sortDirection), sortColumn);
        Page<Person> personPage = personRepository.findAll(pageRequest, "%" + filter + "%");

        System.out.println("page number = " + personPage.getNumber());
        System.out.println("page size = " + personPage.getSize());
        System.out.println("page sort = " + personPage.getSort());
        System.out.println("total pages = " +personPage.getTotalPages());
        System.out.println("total elements = " + personPage.getTotalElements());
        System.out.println("number of elements = " + personPage.getNumberOfElements());

        return personPage;

    }



    @GetMapping("/search")
    public List<Person> search(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "surname") String surname
    ) {
//        return personRepository.findPersonByNameLikeOrSurnameLikeOrderByNameDesc(name, surname);
        return personRepository.findPersonByNameLikeOrSurnameLikeOrderByNameDesc("%" + name + "%", "%" + surname + "%");
    }

    @GetMapping("/name-count")
    public List<NameCount> getNameStatistics() {
        return personRepository.getNameStatistics();
    }

    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable(name = "id") long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if(optionalPerson.isPresent()) {
            return optionalPerson.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + id + " not found!");
        }
    }

    @PostMapping("/")
    public Person addPerson(@RequestBody Person person) {
        return personService.addPerson(person);
    }

    @PutMapping("/{id}")
    public Person savePerson(@PathVariable(name = "id") long id,
                             @RequestBody Person person) {
        return personRepository.save(person);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePerson(@PathVariable(name = "id") long id) {
        if(personRepository.existsById(id)) {
            personRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + id + " not found!");
        }
    }

}
