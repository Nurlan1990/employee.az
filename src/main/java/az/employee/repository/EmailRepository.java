package az.employee.repository;

import az.employee.domain.Email;

public interface EmailRepository {
    Email addEmail(Email email);
    boolean isDuplicate(String email);
}
