package az.employee.repository.jpa;

import az.employee.entity.Author;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    <S extends Author> S save(S entity);

    @Query(value = "select a.* from author a join book_author ba on a.id = ba.author_id" +
            " join book b on ba.book_id = b.id " +
            " where b.id = :book_id", nativeQuery = true)
    List<Author> findAuthorsByBookId(@Param("book_id") long bookId);

    List<Author> findAll();
}
