package az.employee.repository.jpa;

import az.employee.entity.NameCount;
import az.employee.entity.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {

//    @Query("select p from Person p where concat(id, name, surname) like :filter")
    @Query(value = "select * from person_table p where concat(person_id, first_name, last_name) like :filter", nativeQuery = true)
    Page<Person> findAll(Pageable pageable, @Param("filter") String filter);

    @Query(value = "select p.first_name name, count(p.person_id) count " +
            "from person_table p " +
            "group by p.first_name " +
            "order by count(p.person_id) desc", nativeQuery = true)
    List<NameCount> getNameStatistics();

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    Person save(Person person);

    List<Person> findPersonByNameStartingWithAndSurnameStartingWithOrderByIdDesc(String name, String surname);

    List<Person> findPersonByNameContainingOrSurnameContainingOrderByNameDesc(String name, String surname);

    List<Person> findPersonByNameLikeOrSurnameLikeOrderByNameDesc(String name, String surname);
}
