package az.employee.repository.jpa;

import az.employee.entity.Author;
import az.employee.entity.Book;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    <S extends Book> S save(S entity);

    List<Book> findAll();

    @Query(value = "select b.* from author a join book_author ba on a.id = ba.author_id" +
            " join book b on ba.book_id = b.id " +
            " where a.id = :author_id", nativeQuery = true)
    List<Book> findBooksByAuthorId(@Param("author_id") long authorId);

}
