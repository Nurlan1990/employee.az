package az.employee.repository.jdbc;

import az.employee.domain.Role;
import az.employee.domain.User;
import az.employee.repository.UserRepository;
import az.employee.repository.jdbc.mapper.RoleRowMapper;
import az.employee.repository.jdbc.mapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryJdbcImpl implements UserRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private UserRowMapper userRowMapper;

    @Autowired
    private RoleRowMapper roleRowMapper;

    @Override
    public User addUser(User user) {
        // todo impl

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", user.getName())
                .addValue("surname", user.getSurname())
                .addValue("email", user.getEmail())
                .addValue("password", user.getPassword());

//        "insert into user(id, name, surname,  email, password) " +
//                "values(null, :name, :surname, :email, :password)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(SqlQuery.ADD_USER, params, keyHolder);

        if (count > 0) {
            user.setId(keyHolder.getKey().longValue());
        } else {
            throw new RuntimeException("Error adding user " + user + ", count = 0");
        }
        return user;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        Optional<User> optionalUser = Optional.empty();

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("email", email);

        List<User> users = jdbcTemplate.query(SqlQuery.GET_USER_BY_EMAIL, params, userRowMapper);

        if (!users.isEmpty()) {
            optionalUser = Optional.of(users.get(0));
        }

        return optionalUser;
    }

    @Override
    public List<Role> getUserRoles(long userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("user_id", userId);
        return jdbcTemplate.query(SqlQuery.GET_USER_ROLES, params, roleRowMapper);
    }
}
