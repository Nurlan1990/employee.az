package az.employee.repository.jdbc;

import az.employee.domain.Job;
import az.employee.domain.JobType;
import az.employee.domain.Company;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class    JobMapper implements RowMapper<Job> {

    @Override
    public Job mapRow(ResultSet rs, int rowNum) throws SQLException {
        Job job = new Job();

        job.setId(rs.getLong("id"));
        job.setPosition(rs.getString("position"));
        job.setRequirement(rs.getString("requirement"));
        job.setAbout(rs.getString("about"));

        if(rs.getBigDecimal("salary_min") != null) {
            job.setMinSalary(rs.getBigDecimal("salary_min"));
        }

        if(rs.getBigDecimal("salary_max") != null) {
            job.setMaxSalary(rs.getBigDecimal("salary_max"));
        }

        job.setPostDate(rs.getTimestamp("post_date").toLocalDateTime());

        if(rs.getDate("deadline") != null) {
            job.setDeadline(rs.getDate("deadline").toLocalDate());
        }

        job.getAddress().setId(rs.getLong("address_id"));
        job.getAddress().getCountry().setId(rs.getLong("country_id"));
        job.getAddress().getCountry().setName(rs.getString("country_name"));

        job.getAddress().getCity().setId(rs.getLong("city_id"));
        job.getAddress().getCity().setName(rs.getString("city_name"));

        job.getAddress().setInfo(rs.getString("info"));

        job.setType(new JobType(rs.getInt("job_type_id"), rs.getString("job_type_name")));

        if(rs.getInt("experience_min") > 0) {
            job.setMinExperience(rs.getInt("experience_min"));
        }

        if(rs.getInt("experience_max") > 0) {
            job.setMaxExperience(rs.getInt("experience_max"));
        }

        job.getCategory().setId(rs.getLong("job_category_id"));
        job.getCategory().setName(rs.getString("job_category_name"));

        job.getParentCategory().setId(rs.getLong("parent_job_cat_id"));
        job.getParentCategory().setName(rs.getString("parent_job_cat_name"));


        job.setInsertDate(rs.getTimestamp("idate").toLocalDateTime());

        if(rs.getTimestamp("udate") != null) {
            job.setLastUpdate(rs.getTimestamp("udate").toLocalDateTime());
        }

        job.getUser().setId(rs.getLong("user_id"));
        job.getUser().setName(rs.getString("name"));
        job.getUser().setSurname(rs.getString("surname"));
        job.getUser().setEmail(rs.getString("email"));
        job.getUser().setPhone(rs.getString("phone"));
        job.getUser().setMobile(rs.getString("mobile"));

        if(rs.getLong("company_id") > 0) {
            job.setCompany(new Company());
            job.getCompany().setId(rs.getLong("company_id"));
            job.getCompany().setName(rs.getString("company_name"));
            job.getCompany().setEmail(rs.getString("company_email"));
            job.getCompany().setPhone(rs.getString("company_phone"));
            job.getCompany().setMobile(rs.getString("company_mobile"));
        }

        return job;
    }
}
