package az.employee.repository.jdbc;

import az.employee.domain.Job;
import az.employee.domain.JobCategoryCount;
import az.employee.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JobRepositoryJdbcImpl implements JobRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private JobMapper jobMapper;

    @Override
    public long getAllJobCount() {
        return jdbcTemplate.queryForObject(SqlQuery.GET_ALL_JOB_COUNT, new MapSqlParameterSource(), Integer.class);
    }

    @Override
    public List<JobCategoryCount> getJobCategoryCountList() {
        return jdbcTemplate.query(SqlQuery.GET_JOB_CATEGORY_COUNT,
                new MapSqlParameterSource(),
                (rs, rowNum) -> new JobCategoryCount(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getInt("job_count"),
                        rs.getString("icon_class")
                )
        );
    }

    @Override
    public List<Job> getRecentJobs() {
        return jdbcTemplate.query(SqlQuery.GET_RECENT_JOBS, new MapSqlParameterSource(), jobMapper);
    }
/*
* "select * from job j " +
            "join job_category jc on j.job_category_id = jc.id " +
            "where jc.parent_id = :parentId ";
*
* */
    @Override
    public long getJobCategoryCountById(long id) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("parentId",id);
        return jdbcTemplate.queryForObject(SqlQuery.GET_JOB_CATEGORY_COUNT_BY_ID,sqlParameterSource,Long.class);
    }

    @Override
    public List<Job> getJobListByCategoryId(long id, long offset, long pageSize) {

        String sql = SqlQuery.GET_LIST_JOB_BY_CATEGORY_ID.replace("{OFFSET}",String.valueOf(offset))
                .replace("{PAGE_SIZE}", String.valueOf(pageSize));
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("parentID",id);
       return jdbcTemplate.query(sql,sqlParameterSource,jobMapper);
    }

    @Override
    public List<Job> getAllJobList(long offset, long pageSize) {
        System.out.println("getAllJobList offset = " + offset + " page size = " + pageSize);

        String sql = SqlQuery.GET_ALL_JOB_LIST.replace("{OFFSET}",String.valueOf(offset))
                .replace("{PAGE_SIZE}", String.valueOf(pageSize));
       return jdbcTemplate.query(sql,new MapSqlParameterSource(),jobMapper);
    }

    @Override
    public Optional<Job> getJobById(long id) {
        Optional<Job> optionalJob = Optional.empty();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("job_id", id);
        List<Job> jobs = jdbcTemplate.query(SqlQuery.GET_JOB_BY_ID, parameterSource, jobMapper);

        if(!jobs.isEmpty()) {
            optionalJob = Optional.of(jobs.get(0));
        }
        return optionalJob;
    }

    @Override
    public Job addJob(Job job) {
        // todo add job
        System.out.println("job repository add new job " + job);
        return job;
    }

    @Override
    public Job updateJob(Job job) {
        // todo update job
        return job;
    }

    @Override
    public boolean deleteJob(long jobId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("job_id", jobId);
        int count = jdbcTemplate.update(SqlQuery.DELETE_JOB_BY_ID, parameterSource);
        return count == 1;
    }
}
