package az.employee.repository.jdbc;

import az.employee.domain.Token;
import az.employee.repository.TokenRepository;
import az.employee.repository.jdbc.mapper.TokenRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TokenRepositoryJdbcImpl implements TokenRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private TokenRowMapper tokenRowMapper;

    @Override
    public Token addToken(Token token) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("value", token.getValue())
                .addValue("token_type_id", token.getType().getValue())
                .addValue("generation_date", token.getGenerationDate())
                .addValue("expire_date", token.getExpireDate())
                .addValue("user_id", token.getUser().getId());

        int count = jdbcTemplate.update(SqlQuery.ADD_TOKEN, params);
        if(count > 0) {
            // todo add logging
            System.out.println("token insert success");
        } else {
            throw new RuntimeException("Error adding token, insert count = 0");
        }
        return token;
    }

    @Override
    public Optional<Token> getToken(String token) {
        Optional<Token> optionalToken = Optional.empty();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("token", token);
        List<Token> list = jdbcTemplate.query(SqlQuery.GET_TOKEN, params, tokenRowMapper);
        if(!list.isEmpty()) {
            optionalToken = Optional.of(list.get(0));
        }

        return optionalToken;
    }
}
