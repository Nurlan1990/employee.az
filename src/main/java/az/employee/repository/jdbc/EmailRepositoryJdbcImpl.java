package az.employee.repository.jdbc;

import az.employee.domain.Email;
import az.employee.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class EmailRepositoryJdbcImpl implements EmailRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Email addEmail(Email email) {
        // todo implement add email
        return email;
    }

    @Override
    public boolean isDuplicate(String email) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("email", email);
        int count = jdbcTemplate.queryForObject(SqlQuery.CHECK_EMAIL, params, Integer.class);
        return count > 0;
    }
}
