package az.employee.repository.jdbc.mapper;

import az.employee.domain.Country;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
@Component
public class CountryMapper implements RowMapper<Country> {

    @Override
    public Country mapRow(ResultSet resultSet, int i) throws SQLException {
        Country country =  new Country();
        country.setId(resultSet.getLong("id"));
        country.setName(resultSet.getString("name"));
        return country;
    }
}
