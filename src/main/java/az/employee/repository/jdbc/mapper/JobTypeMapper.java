package az.employee.repository.jdbc.mapper;

import az.employee.domain.JobType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class JobTypeMapper implements RowMapper<JobType> {
    @Override
    public JobType mapRow(ResultSet resultSet, int i) throws SQLException {
        JobType jobType = new JobType();
        jobType.setId(resultSet.getLong("id"));
        jobType.setName(resultSet.getString("name"));
        return jobType;
    }
}
