package az.employee.repository.jdbc.mapper;

import az.employee.domain.Job;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class CompanyJobAjaxMapper implements RowMapper<Job> {

    /*
    *
    * " select concat(u.name ,\" \", u.surname) as editor , j.position , j.about,jc.name job_category_name ,requirement , j.deadline,\n" +
           "concat(c.name,\" \",cy.name,\" \", a.info) as Address from job j " +
           "join user u on u.id = j.user_id\n" +
           "join address a on a.id = j.address_id\n" +
           "join country c on c.id = a.country_id and c.id = :companyId" +
           "join city cy on cy.id = a.city_id\n" +
           "join job_category jc on jc.id = j.job_category_id\n" +
           "where concat(concat(u.name ,\" \", u.surname),j.position,j.about,jc.name,requirement,j.deadline) Like :condition " +
           "order by {column_name} {direction} " +
           "Limit {offset} , {length}  ";

    *
    *
    *
    *
    *
    *
    *
    * */
    @Override
    public Job mapRow(ResultSet resultSet, int i) throws SQLException {
        Job job = new Job();
        job.setId(resultSet.getLong("id"));
        job.getUser().setName(resultSet.getString("editor"));
        job.setPosition(resultSet.getString("position"));
        job.getCategory().setName(resultSet.getString("job_category_name"));
        job.setDeadline(resultSet.getDate("deadline").toLocalDate());
        job.getAddress().setInfo(resultSet.getString("Address"));
        return job;
    }
}
