package az.employee.repository.jdbc;

public class SqlQuery {

    public static final String ADD_USER = "insert into user(id, name, surname , email, password) " +
            "values(null, :name, :surname, :email, :password)";

    public static final String GET_USER_BY_EMAIL = "select id, name, surname, phone , mobile , user_status_id, " +
            "   email, password, idate, udate " +
            " from user " +
            "where email = :email and status = 1";

    public static final String GET_USER_ROLES = "select r.id, r.name " +
            "from user_role ur join user u on ur.user_id = u.id " +
            "   join role r on ur.role_id = r.id " +
            "where u.id = :user_id";

    public static final String ADD_TOKEN = "insert into token(id, value, token_type_id, generation_date, expire_date, user_id) " +
            "values(null, :value, :token_type_id, :generation_date, :expire_date, :user_id)";

    public static final String GET_TOKEN = "select id, value, token_type_id, generation_date, expire_date, " +
            "user_id, used, idate, udate " +
            "from token " +
            "where value = :token and status = 1";

    public static final String CHECK_EMAIL = "select count(id) cnt from user " +
            "where email = :email and status = 1";

    public static final String GET_JOB_CATEGORY_COUNT = "select jcp.id, jcp.name, jcp.icon_class, count(j.id) job_count " +
            "from job j join job_category jc on j.job_category_id = jc.id and jc.status = 1\n" +
            "\tjoin job_category jcp on jc.parent_id = jcp.id and jcp.status = 1\n" +
            "group by jcp.id, jcp.name, jcp.icon_class\n" +
            "order by count(j.id) desc\n" +
            "limit 5";

    public static final String GET_ALL_JOB_COUNT = "select count(j.id) job_count " +
            "from job j " +
            "where j.status = 1";

    public static final String GET_RECENT_JOBS = "select j.id, j.position, j.about, j.requirement,\n" +
            "\tj.salary_min, j.salary_max, j.post_date, j.deadline,\n" +
            "    j.address_id, a.country_id, cr.name country_name,\n" +
            "    a.city_id, ct.name city_name, a.info,\n" +
            "    j.job_type_id, jt.name job_type_name,\n" +
            "    j.experience_min, j.experience_max,\n" +
            "    j.job_category_id, jc.name job_category_name,\n" +
            "    jcp.id parent_job_cat_id, jcp.name parent_job_cat_name,\n" +
            "    j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
            "    j.company_id, o.name company_name, " +
            "    o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
            "    j.idate, j.udate\n" +
            "from job j join user u on j.user_id = u.id and u.status = 1\n" +
            "\tleft join company o on j.company_id = o.id and o.status = 1\n" +
            "    join job_category jc on jc.id = j.job_category_id and jc.status = 1\n" +
            "    join job_category jcp on jc.parent_id = jcp.id and jcp.status = 1\n" +
            "    join address a on j.address_id = a.id and a.status = 1\n" +
            "    join country cr on a.country_id = cr.id and cr.status = 1\n" +
            "    join city ct on a.city_id = ct.id and ct.status = 1\n" +
            "    join job_type jt on j.job_type_id = jt.id and jt.status = 1\n" +
            "where j.status = 1\n" +
            "order by post_date desc\n" +
            "limit 10";

    public static final String GET_JOB_CATEGORY_COUNT_BY_ID = "select count(j.id) from job j " +
            "join job_category jc on j.job_category_id = jc.id " +
            "where jc.parent_id = :parentId ";


    public static final String GET_LIST_JOB_BY_CATEGORY_ID = " select j.id, j.position, j.about, j.requirement,\n" +
            "            j.salary_min, j.salary_max, j.post_date, j.deadline,\n" +
            "               j.address_id, a.country_id, cr.name country_name,\n" +
            "                a.city_id, ct.name city_name, a.info,\n" +
            "                j.job_type_id, jt.name job_type_name,\n" +
            "                j.experience_min, j.experience_max,\n" +
            "\t\t\tj.job_category_id, jc.name job_category_name,\n" +
            "               jcp.id parent_job_cat_id, jcp.name parent_job_cat_name,\n" +
            "              j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
            "\t\t    j.company_id, o.name company_name, \n" +
            "                o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
            "                j.idate, j.udate\n" +
            "            from job j join user u on j.user_id = u.id and u.status = 1\n" +
            "            left join company o on j.company_id = o.id and o.status = 1\n" +
            "                join job_category jc on jc.id = j.job_category_id and jc.status = 1\n" +
            "                join job_category jcp on jc.parent_id = jcp.id and jc.parent_id = :parentID \n" +
            "                and jcp.status = 1\n" +
            "                join address a on j.address_id = a.id and a.status = 1\n" +
            "                join country cr on a.country_id = cr.id and cr.status = 1\n" +
            "                join city ct on a.city_id = ct.id and ct.status = 1\n" +
            "                join job_type jt on j.job_type_id = jt.id and jt.status = 1\n" +
            "            where j.status = 1\n" +
            "            order by post_date desc " +
            " LIMIT {OFFSET}, {PAGE_SIZE}";

    public static final String GET_ALL_JOB_LIST = " select j.id, j.position, j.about, j.requirement,\n" +
            "            j.salary_min, j.salary_max, j.post_date, j.deadline,\n" +
            "               j.address_id, a.country_id, cr.name country_name,\n" +
            "                a.city_id, ct.name city_name, a.info,\n" +
            "                j.job_type_id, jt.name job_type_name,\n" +
            "                j.experience_min, j.experience_max,\n" +
            "\t\t\tj.job_category_id, jc.name job_category_name,\n" +
            "               jcp.id parent_job_cat_id, jcp.name parent_job_cat_name,\n" +
            "              j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
            "\t\t    j.company_id, o.name company_name, \n" +
            "                o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
            "                j.idate, j.udate\n" +
            "            from job j join user u on j.user_id = u.id and u.status = 1\n" +
            "            left join company o on j.company_id = o.id and o.status = 1\n" +
            "                join job_category jc on jc.id = j.job_category_id and jc.status = 1\n" +
            "                join job_category jcp on jc.parent_id = jcp.id and jcp.status = 1\n" +
            "                join address a on j.address_id = a.id and a.status = 1\n" +
            "                join country cr on a.country_id = cr.id and cr.status = 1\n" +
            "                join city ct on a.city_id = ct.id and ct.status = 1\n" +
            "                join job_type jt on j.job_type_id = jt.id and jt.status = 1\n" +
            "            where j.status = 1\n" +
            "            order by post_date desc " +
            " LIMIT {OFFSET}, {PAGE_SIZE}";

    public static final String GET_JOB_BY_ID = " select j.id, j.position, j.about, j.requirement,\n" +
            "            j.salary_min, j.salary_max, j.post_date, j.deadline,\n" +
            "               j.address_id, a.country_id, cr.name country_name,\n" +
            "                a.city_id, ct.name city_name, a.info,\n" +
            "                j.job_type_id, jt.name job_type_name,\n" +
            "                j.experience_min, j.experience_max,\n" +
            "\t\t\tj.job_category_id, jc.name job_category_name,\n" +
            "               jcp.id parent_job_cat_id, jcp.name parent_job_cat_name,\n" +
            "              j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
            "\t\t    j.company_id, o.name company_name, \n" +
            "                o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
            "                j.idate, j.udate\n" +
            "            from job j join user u on j.user_id = u.id and u.status = 1\n" +
            "            left join company o on j.company_id = o.id and o.status = 1\n" +
            "                join job_category jc on jc.id = j.job_category_id and jc.status = 1\n" +
            "                join job_category jcp on jc.parent_id = jcp.id and jcp.status = 1\n" +
            "                join address a on j.address_id = a.id and a.status = 1\n" +
            "                join country cr on a.country_id = cr.id and cr.status = 1\n" +
            "                join city ct on a.city_id = ct.id and ct.status = 1\n" +
            "                join job_type jt on j.job_type_id = jt.id and jt.status = 1\n" +
            "            where j.id = :job_id and j.status = 1";

    public static final String GET_CANDIDATE_LIST_WITH_PAGING = "select c.id, u.id user_id, u.name, u.surname, u.udate,u.idate,u.user_status_id,u.email,u.password,u.phone,u.mobile, " +
            "            c.birth_date, c.profile_image, c.video_file, " +
            "            c.address_id, a.info, " +
            "            a.country_id, cr.name country_name, " +
            "            a.city_id, ct.name city_name, " +
            "            c.degree_id, el.name education_level, " +
            "            c.position, c.salary_min, c.salary_max, " +
            "            c.github, c.facebook, c.twitter, c.linkedin, c.instagram,c.gitlab, " +
            "            c.idate, c.udate " +
            "from candidate c join user u on c.user_id = u.id and u.status = 1\n" +
            "\tjoin address a on c.address_id = a.id and a.status = 1\n" +
            "    join country cr on cr.id = a.country_id and cr.status = 1\n" +
            "    join city ct on ct.id = a.city_id and ct.status = 1\n" +
            "    left join education_level el on c.degree_id = el.id and el.status = 1\n" +
            "where c.status = 1\n" +
            "order by id desc\n" +
            "limit {OFFSET}, {PAGE_SIZE}";

    public static final String DELETE_JOB_BY_ID = "update job " +
            "set status = 0 " +
            "where id = :job_id and status = 1";

    public static String GET_CANDIDATE_COUNT = "select count(id) " +
            "from candidate " +
            "where status = 1";

    public static String GET_COMPANY_BY_USER_ID = " select c.id, c.name , c.industry_id, c.head_office , c.about, c.create_date , c.num_of_employee , c.annual_revenue,\n" +
            " c.is_global,c.rating, c.phone , c.mobile , c.email , c.website \n" +
            " from company_user cu\n" +
            " join user u on u.id = cu.user_id and u.id = :userId and u.status = 1 \n" +
            " join company c on c.id=cu.company_id and c.status = 1 ";

    public static String GET_COMPANY_JOB_COUNT_BY_COMPANY_ID = "select count(id) from job where company_id = :companyId ";

    public  static  String GET_COMPANY_JOB_LIST_BY_PAGE = " select j.id , j.position , j.about , j.requirement , j.salary_min , j.address_id , a.country_id , a.city_id," +
            " j.salary_max , j.post_date , j.deadline , cr.name country_name , ci.name city_name , j.job_type_id , a.info ,jt.name job_type_name,\n" +
            " j.experience_min,j.experience_max , j.job_category_id , jcp.id parent_job_cat_id , jc.name job_category_name , jcp.name parent_job_cat_name ," +
            " o.name company_name\n" +
            ",j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
            "  o.id company_id , o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
            "  j.idate, j.udate\n " +
            " from job j\n" +
            " join user u on u.id = j.user_id "+
            " join company o on o.id = j.company_id\n" +
            " join job_category jc on jc.id = j.job_category_id\n" +
            " join job_category jcp on jcp.id = jc.parent_id\n" +
            " join job_type jt on jt.id = j.job_type_id\n" +
            " join address a on j.address_id = a.id and  j.company_id = :companyId \n" +
            " join country cr on cr.id = a.country_id\n" +
            " join city ci on ci.id = a.city_id " +
            " LIMIT {OFFSET} , {LENGTH}";


    public static String GET_JOB_CATEGORIES  = "  select id , name from job_category \n" +
            " where parent_id is not null ";

    public static final String GET_COUNTRY_LIST = "select id, name  " +
            "from country " +
            "where status = 1";

    public static final String GET_CITY_LIST = "select id, name, country_id" +
            "  from city " +
            "where status = 1";

    public static final String GET_CITY_LIST_BY_COUNTRY_ID = "select id, name, country_id" +
            "  from city " +
            "where status = 1 and country_id = :country_id";

    public static  String INSERT_JOB = "Insert into job(user_id ,salary_min ,salary_max," +
            " deadline,experience_min,experience_max,job_category_id, " +
            " address_id, position , about , requirement , job_type_id,company_id) " +
            "Values ( :userId,:salaryMin,:salaryMax, :deadline, :experienceMin , :experienceMax ,:jobCategoryId, " +
            " :addressId ,:position,:about,:requirement , :jobTypeId , :companyId) ";

   public  static final  String GET_JOB_TYPE_LIST =" select id , name  from job_type ";
   public static final String GET_JOB_CATEGORY = " select id , name  from job_category where parent_id is not null ";
   public static final String INSERT_ADDRESS = " insert into address(id,country_id,city_id,info) " +
           " Values (null,:countryId,:cityId,:info); ";

   public static final String  GET_COMPANY_JOB_WITH_AJAX = " select j.id , concat(u.name ,\" \", u.surname) as editor , j.position ,jc.name job_category_name , j.deadline,\n" +
           "concat(c.name,\" \",cy.name,\" \", a.info) as Address from job j " +
           "join user u on u.id = j.user_id\n and j.company_id = :companyId and j.status = 1 " +
           "join address a on a.id = j.address_id\n" +
           "join country c on c.id = a.country_id " +
           "join city cy on cy.id = a.city_id\n" +
           "join job_category jc on jc.id = j.job_category_id " +
           "where concat(concat(u.name ,\" \", u.surname),j.position,jc.name,j.deadline ,concat(c.name,\" \",cy.name,\" \", a.info)) Like :condition " +
           "order by {column_name} {direction} " +
           "Limit {offset} , {length}  ";

   public static final String GET_FILTERED_COMPANY_JOB_COUNT =" select count(j.position) from job j " +
           "join user u on u.id = j.user_id\n and j.company_id = :companyId " +
           "join address a on a.id = j.address_id\n" +
           "join country c on c.id = a.country_id " +
           "join city cy on cy.id = a.city_id\n" +
           "join job_category jc on jc.id = j.job_category_id " +
           "where concat(concat(u.name ,\" \", u.surname),j.position,jc.name,j.deadline ,concat(c.name,\" \",cy.name,\" \", a.info)) Like :condition " ;

   public static final String GET_SINGLE_JOB_BY_COMPANY_ID_AND_JOB_ID = " select j.id , j.position , j.about , j.requirement , j.salary_min , j.address_id , a.country_id , a.city_id," +
           " j.salary_max , j.post_date , j.deadline , cr.name country_name , ci.name city_name , j.job_type_id , a.info ,jt.name job_type_name,\n" +
           " j.experience_min,j.experience_max , j.job_category_id , jcp.id parent_job_cat_id , jc.name job_category_name , jcp.name parent_job_cat_name ," +
           " o.name company_name\n" +
           ",j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
           "  o.id company_id , o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
           "  j.idate, j.udate\n " +
           " from job j\n" +
           " join user u on u.id = j.user_id and j.id = :jobId "+
           " join company o on o.id = j.company_id\n" +
           " join job_category jc on jc.id = j.job_category_id\n" +
           " join job_category jcp on jcp.id = jc.parent_id\n" +
           " join job_type jt on jt.id = j.job_type_id\n" +
           " join address a on j.address_id = a.id and  j.company_id = :companyId \n" +
           " join country cr on cr.id = a.country_id\n" +
           " join city ci on ci.id = a.city_id " ;

   public static final String UPDATE_JOB = "update job set position = :position , about = :about ,requirement = :requirement,salary_min=:salaryMin,salary_max = :salaryMax,deadline = :deadline,\n" +
           " job_type_id = :jobTypeId , experience_min = :experienceMin , experience_max = :experienceMax ,  job_category_id = :jobCategoryId where id = :jobId ";

   public static final String UPDATE_ADDRESS = "update address set country_id = :countryId , city_id = :cityId , info = :info where id = :addressId ";

public static final String GET_INDUSTRY_LIST = "select id , name  from industry  ";

public static final String GET_COMPANY_BY_COMPANY_ID = "select id , name , industry_id,head_office,create_date,num_of_employee,\n" +
        " annual_revenue,is_global , rating,about,phone,mobile,website,email from company  \n" +
        " where id = :id ";

public static final String UPDATE_COMPANY_BY_ID = "update company set name = :name , industry_id = :industryId , head_office = :headOffice , create_date = :createDate , num_of_employee = :numOfEmployee,\n" +
        "annual_revenue = :annualRevenue , is_global = :isGlobal , rating = :rating , about = :about , phone = :phone , mobile=:mobile , website = :website,\n" +
        "email = :email where id = :id   ";

public static final String DELETE_JOB_BY_ID_AND_COMPANY_ID = "update job set status = 0 where id =:id " +
        " and company_id = :companyId  ";

    public static String GET_CANDIDATE_JOB_HISTORY_LIST = "select jh.id, jh.candidate_id, jh.position, jh.start_date, jh.end_date, jh.country_id, cr.name country_name, " +
            "jh.city_id, ct.name city_name, jh.company, jh.info, jh.idate, jh.udate " +
            "from job_history jh " +
            "join candidate c on jh.candidate_id = c.id and c.status = 1 " +
            "join country cr on jh.country_id = cr.id and jh.status = 1 and cr.status = 1 " +
            "join city ct on jh.city_id = ct.id and ct.status = 1 " +
            "where jh.candidate_id = :candidate_id";


    public static String GET_JOB_HISTORY_BY_ID = "select jh.id, jh.candidate_id, jh.position, jh.start_date, jh.end_date, jh.country_id, cr.name country_name, " +
            "jh.city_id, ct.name city_name, jh.company, jh.info, jh.idate, jh.udate " +
            "from job_history jh " +
            "join candidate c on jh.candidate_id = c.id and c.status = 1 " +
            "join country cr on jh.country_id = cr.id and cr.status = 1 " +
            "join city ct on jh.city_id = ct.id and ct.status = 1 " +
            "where jh.id = :job_history_id and jh.status = 1";

    public static String UPDATE_JOB_HISTORY_BY_ID = "update job_history" +
            "set start_date = :start, end_date = :end_date, " +
            "   company = :company, position = :position, " +
            "   country_id = :country_id, city_id = :city_id, " +
            "   info = :info " +
            "where id = :job_history_id and status = 1";

    public static String DELETE_JOB_HISTORY_BY_ID = "update job_history " +
            "set status = 0 " +
            "where id = :job_history_id and status = 1";

    public static String ADD_JOB_HISTORY = "insert into job_history(id, candidate_id, position, start_date, end_date, " +
            "   country_id, city_id, company, info) " +
            "values(null, :candidate_id, :position, :start_date, :end_date, " +
            "   :country_id, :city_id, :company, :info)";

    public static final  String GET_CANDIDATE_LIST_ADMIN = "select c.id, u.id user_id, u.name, u.surname, u.udate,u.idate,u.user_status_id,u.email,u.password,u.phone,u.mobile, " +
            "        c.birth_date, c.profile_image, c.video_file, " +
            "        c.address_id, ad.info, " +
            "        ad.country_id, cr.name country_name, " +
            "        ad.city_id, ct.name city_name, " +
            "        c.degree_id, el.name education_level, " +
            "        c.position, c.salary_min, c.salary_max, " +
            "        c.github, c.facebook, c.twitter, c.linkedin, c.instagram,c.gitlab, " +
            "        c.idate, c.udate " +
            "        from user u  join candidate c on c.user_id = u.id and u.status = 1 " +
            "        join address ad on c.address_id = ad.id and ad.status = 1 " +
            "        join country cr on ad.country_id = cr.id and cr.status = 1 " +
            "        join city ct on ad.city_id = ct.id and ct.status = 1 " +
            "        left join education_level el on c.degree_id = el.id and el.status = 1 " +
            "        join user_role ur on ur.user_id = u.id and ur.status = 1 " +
            "        where ur.role_id = 1 and concat(u.name,u.surname,u.email,ifnull(u.mobile,\"\"),ifnull(c.position,\"\"),ifnull(c.salary_min,\"\"),ifnull(c.salary_max,\"\"),cr.name,ct.name) like :searchWord " +
            "        order by %s %s " +
            "        limit %d, %d ";


    public static final String GET_CANDIDATE_BY_ID = "select u.name,u.surname,u.user_status_id,u.email,u.password,u.phone,u.mobile, " +
            "        c.id, c.user_id,c.birth_date,c.profile_image,c.video_file,c.position,c.salary_min,c.salary_max, " +
            "        c.address_id,ad.info, " +
            "        ad.city_id,ct.name city_name, " +
            "        ad.country_id,cr.name country_name, " +
            "        c.degree_id,el.name education_level, " +
            "        c.github, c.facebook, c.twitter, c.linkedin, c.instagram,c.gitlab, " +
            "        c.idate, c.udate " +
            "        from candidate c join user u on c.user_id = u.id and c.status = 1 and u.status = 1 " +
            "        join address ad on c.address_id = ad.id and ad.status = 1 " +
            "        join country cr on ad.country_id = cr.id and cr.status = 1 " +
            "        join city ct on ad.city_id = ct.id and ct.status = 1 " +
            "        left join education_level el on c.degree_id = el.id and el.status = 1 " +
            "        where c.id = :candidate_id ";

    public static final String GET_EDUCATIONS_LIST = "select e.id, e.candidate_id, e.education_level_id, " +
            "            el.name edu_level_name, e.start_date, e.end_date, e.country_id, cr.name country_name, " +
            "            e.education_org_id, eo.name edu_org_name, e.education_org_name,  e.graduated, e.idate, e.udate " +
            "            from education e " +
            "            join candidate c on e.candidate_id = c.id and e.status = 1 and c.status = 1 " +
            "            join education_level el on e.education_level_id = el.id and el.status = 1 " +
            "            join country cr on e.country_id = cr.id and cr.status = 1 " +
            "            join education_org eo on e.education_org_id = eo.id and eo.status = 1 " +
            "            where candidate_id = :candidate_id";

    public static final String GET_CERTIFICATE_LIST = "select cert.id, cert.candidate_id, cert.certificate_date, " +
            "            cert.title, cert.organization, cert.file, cert.idate, cert.udate " +
            "            from certificate cert " +
            "            join candidate c on cert.candidate_id = c.id and cert.status = 1 and c.status = 1 " +
            "            where cert.candidate_id = :candidate_id ";

    public static final String GET_LANGUAGES_SKILLS = "select ls.id, ls.candidate_id, ls.language_id, " +
            "            l.name language_name, ls.reading, ls.speaking, ls.writing, ls.listening, ls.idate, ls.udate " +
            "            from language_skill ls " +
            "            join candidate c on ls.candidate_id = c.id and ls.status = 1 and c.status = 1 " +
            "            join language l on ls.language_id = l.id and l.status = 1 " +
            "            where candidate_id = :candidate_id ";

    public static final String GET_CANDIDATE_SKILL_LIST = "select s.id, s.candidate_id, s.name, s.level, s.info, s.idate, s.udate " +
            "from skill s " +
            "join candidate c on s.candidate_id = c.id and s.status = 1 and c.status = 1 " +
            "where s.candidate_id = :candidate_id";

    public static final String GET_CANDIDATE_TAGS = "select t.id, t.name, t.idate, t.udate " +
            "  from tag_owner tr join candidate c on tr.owner_id = c.id and tr.status = 1 and c.status = 1 " +
            "  join tag t on tr.tag_id = t.id and t.status = 1 " +
            "  where tr.owner_id = :candidate_id ";

    public static final String SAVE_CANDIDATE_PROFILE_IMAGE = "update candidate " +
            "set profile_image = :image " +
            "where id = :id and status = 1";
    public static final String GET_CANDIDATE_N_JOB_LIST_WITH_PAGING = "select u.id user_id, u.name, u.surname, \n" +
            "j.position, u.email, c.profile_image, c.id candidate_id, j.id job_id\n" +
            "from job_user ju join job j on ju.job_id=j.id and ju.status=1 and j.status=1 " +
            "join user u on u.id = ju.user_id and u.status=1\n" +
            "join candidate c on c.user_id=u.id and c.status=1\n" +
            "where j.user_id=:userId" +
            " limit {OFFSET}, {PAGE_SIZE}";

    public static final String CAN_APP_COM_FOR_AJAX_COUNT="select count(distinct ju.user_id)\n" +
            "from job_user ju \n" +
            "where ju.status=1 and ju.job_id in(\n" +
            "   select j.id\n" +
            "   from job j\n" +
            "   where j.status=1 and j.user_id=:userId " +
            ")";
    public static final String  CAN_APP_COM_FOR_AJAX_FILTERED_COUNT="select count(distinct ju.user_id)\n" +
            "            from job_user ju join user u on u.id=ju.user_id and u.status=1\n" +
            "            join candidate c on c.user_id=u.id and c.status=1\n" +
            "            join address a on a.id=c.address_id and a.status=1\n" +
            "            join city ct on a.city_id=ct.id and ct.status=1 "+
            "            where  concat(u.name, u.surname, u.email, ifnull(u.phone,''), ifnull(u.mobile,''), a.info, ct.name) like :filter " +
            "            and ju.status=1 and ju.job_id in(\n" +
            "               select j.id\n" +
            "               from job j\n" +
            "               where j.status=1 and j.user_id=:userId\n" +
            "            )";

    public static final String GET_APP_CANDIDATE_LIST_PAGING="\n" +
            "select u.id, u.name, u.surname, u.email, u.phone, u.mobile, \n" +
            "            ct.name cityName, cr.name countryName, a.info, c.id candidateId \n" +
            "            from user u join candidate c on u.id=c.user_id and u.status =1 and c.status=1\n" +
            "            join address a on a.id=c.address_id and a.status=1\n" +
            "            join city ct on a.city_id=ct.id and ct.status=1\n" +
            "            join country cr on a.country_id = cr.id and cr.status=1\n" +
            "            where concat(u.name, u.surname, u.email, ifnull(u.phone,''), ifnull(u.mobile,''), a.info, ct.name) like :filter\n" +
            "            and u.id in(\n" +
            "               select user_id\n" +
            "\t\t\t   from job_user\n" +
            "               where job_user.status=1 and job_id in(\n" +
            "                  select id\n" +
            "                  from job\n" +
            "                  where job.status=1 and user_id=3\n" +
            "               )\n" +
            "            )\n" +
            " order by {SORT_COLUMN} {SORT_DIRECTION} " +
            " limit {START}, {LENGTH}";

    public static final String DELETE_CANDIDATE_ALL_APPS="update job_user\n" +
            "set status = 0\n" +
            "where user_id=:userId";
    public static final String GET_ID_FROM_JOB_USER ="select id\n"+
            "from job_user\n"+
            "where user_id=:userId and job_id=:jobId";
    public static final String DELETE_CANDIDATE_APP="update job_user\n" +
            "set status = 0\n" +
            "where id=:id";

    public static final String GET_JOB_USER_COUNT="select count(ju.id) " +
            "from job_user ju  " +
            "where ju.status=1 and job_id in( " +
            "   select j.id " +
            "   from job j " +
            "   where j.user_id=:userId and j.status=1 " +
            ")";



}
