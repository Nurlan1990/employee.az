package az.employee.repository;

import az.employee.domain.Job;
import az.employee.domain.JobCategoryCount;

import java.util.List;
import java.util.Optional;

public interface JobRepository {
    long getAllJobCount();
    List<JobCategoryCount> getJobCategoryCountList();
    List<Job> getRecentJobs();
    long getJobCategoryCountById(long id);
    List<Job> getJobListByCategoryId(long id,long offset, long pageSize);
    List<Job> getAllJobList(long offset, long pageSize);

    Optional<Job> getJobById(long id);
    Job addJob(Job job);
    Job updateJob(Job job);
    boolean deleteJob(long jobId);
}
