package az.employee.util;

public enum CompanyJobDataTableColumnNames {
    EDITOR(0),
    POSITION(1),
    job_category_name(2),
    DEADLINE(3),
    Address(4);


    public int getValue() {
        return value;
    }

    CompanyJobDataTableColumnNames(int value) {
        this.value = value;
    }
    private int value;


    public static CompanyJobDataTableColumnNames whichColumn(int value){
        CompanyJobDataTableColumnNames companyJobDataTableColumnNames = null;
        if (EDITOR.getValue()==value){
            companyJobDataTableColumnNames = EDITOR;
        }
        else if (POSITION.getValue() == value){
            companyJobDataTableColumnNames =  POSITION;
        }
        else if (Address.getValue() == value){
            companyJobDataTableColumnNames =  Address;
        }
        else if (DEADLINE.getValue() == value){
            companyJobDataTableColumnNames =  DEADLINE;
        }
        else if (job_category_name.getValue() == value){
            companyJobDataTableColumnNames =  job_category_name;
        }
        return companyJobDataTableColumnNames;
    }

}
