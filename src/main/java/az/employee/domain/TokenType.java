package az.employee.domain;

import java.util.Arrays;

public enum TokenType {
    ACTIVATION(1), RESET_PASSWORD(2);

    private int value;

    TokenType(int value) {
        this.value = value;
    }

    public static TokenType fromValue(int value) {
        return Arrays.stream(values())
                .filter(tokenType -> tokenType.value == value)
                .findFirst().orElseThrow(() -> new RuntimeException("Invalid token type " + value));
    }

    public int getValue() {
        return value;
    }
}
