package az.employee.domain;

import java.util.Arrays;

public enum UserStatus {
    PENDING(0), ACTIVE(1), DELETED(2), LOCKED(3);

    private int value;

    UserStatus(int value) {
        this.value = value;
    }

    public static UserStatus fromValue(int status) {

        return Arrays.stream(values())
                .filter(us -> us.value == status)
                .findFirst().orElseThrow(() -> new RuntimeException("Invalid user status " + status));

    }
}
