package az.employee.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class BaseDomainClass implements Serializable {
    private static final long serialVersionUID = 4667021688525832058L;
    protected long id;
    protected LocalDateTime insertDate;
    protected LocalDateTime lastUpdate;

    public BaseDomainClass(long id) {
        this.id = id;
    }

    public BaseDomainClass() {
        this(0l);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
