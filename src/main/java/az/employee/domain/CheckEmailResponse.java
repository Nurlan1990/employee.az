package az.employee.domain;

public class CheckEmailResponse {
    private String email;
    private boolean duplicate;
    private String message;

    public CheckEmailResponse(String email, boolean duplicate, String message) {
        this.email = email;
        this.duplicate = duplicate;
        this.message = message;
    }

    public CheckEmailResponse() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean dublicate) {
        this.duplicate = dublicate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CheckEmailResponse{" +
                "email='" + email + '\'' +
                ", dublicate=" + duplicate +
                ", message='" + message + '\'' +
                '}';
    }
}
