package az.employee.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

public class Company extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = 3175247759873076788L;
    private String name;
    private String email;
    private String phone;
    private String mobile;
    private String about;
    private Industry industry;

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", mobile='" + mobile + '\'' +
                ", about='" + about + '\'' +
                ", industry=" + industry +
                ", headOffice='" + headOffice + '\'' +
                ", numberOfEmployees=" + numberOfEmployees +
                ", website='" + website + '\'' +
                ", annualRevenue=" + annualRevenue +
                ", isGlobal=" + isGlobal +
                ", rating=" + rating +
                ", createDate='" + createDate + '\'' +
                '}';
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    private String headOffice;
    private long numberOfEmployees;
    private String website;
    private BigDecimal annualRevenue;
    private boolean isGlobal;
    private long rating;
    private String createDate;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getHeadOffice() {
        return headOffice;
    }

    public void setHeadOffice(String headOffice) {
        this.headOffice = headOffice;
    }

    public long getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(long numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BigDecimal getAnnualRevenue() {
        return annualRevenue;
    }

    public void setAnnualRevenue(BigDecimal annualRevenue) {
        this.annualRevenue = annualRevenue;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public void setGlobal(boolean global) {
        isGlobal = global;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public Company(long id, String name) {
        super(id);
        this.name = name;
    }

    public Company() {
        this(0, "");
        this.industry = new Industry();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
