package az.employee.domain;

import java.io.Serializable;
import java.util.Objects;

public class EducationLevel extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = -2735863491419012583L;
    private String name;

    public EducationLevel(long id, String name) {
        super(id);
        this.name = name;
    }

    public EducationLevel() {
        this(0L, "");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EducationLevel that = (EducationLevel) o;
        return that.id == id && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "EducationLevel{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", insertDate=" + insertDate +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
