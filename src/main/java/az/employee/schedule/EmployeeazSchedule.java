package az.employee.schedule;

import az.employee.config.EmployeeazConfiguration;
import az.employee.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;

@Service
public class EmployeeazSchedule {

    @Autowired
    private EmailService emailService;

    @Autowired
    private EmployeeazConfiguration configuration;

   // @Scheduled(fixedRate = 10 * 1000)
    public void sendSmsNotifications() {
        System.out.println(LocalDateTime.now() + " send sms notifications ");
    }

   // @Scheduled(fixedRate = 15 * 1000)
    public void sendEmailNotifications() {
        System.out.println(LocalDateTime.now() + " send email notifications ");

//        Arrays.asList("elvinprog@gmail.com", "jhunabd@gmail.com",
//                "nguliyeva2020@ada.edu.az", "elchinbba@mail.ru").forEach(email -> {
//            emailService.sendEmail(
//                    configuration.getFrom(),
//                    email,
//                    "hello from employee.az",
//                    "test email from employee.az :) \n "
//                            + " click to activate your profile: http://localhost:9090/activate?token=asdakjsdhaikzsdkasjhdajskldh \n"
//                            + LocalDateTime.now()
//            );
//        });

    }

   // @Scheduled(cron = "*/20 * * * * *")
    public void sendPushNotifications() {
        System.out.println(LocalDateTime.now() + " send push notifications ");
    }


}
