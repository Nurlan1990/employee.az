package az.employee.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "employeeaz")
public class EmployeeazConfiguration {

    private String from;
    private String baseUrl;
    private String activationUrl;
    private Counts counts;
//    private int jobsPageSize;
//    private int candidatesPageSize;
    private String uploadFolder;
    private String[] origins;
    private List<SmsProvider> smsProviders;
    private Map<String, SmsProvider> smsProviderMap;


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Map<String, SmsProvider> getSmsProviderMap() {
        return smsProviderMap;
    }

    public void setSmsProviderMap(Map<String, SmsProvider> smsProviderMap) {
        this.smsProviderMap = smsProviderMap;
    }

    public List<SmsProvider> getSmsProviders() {
        return smsProviders;
    }

    public void setSmsProviders(List<SmsProvider> smsProviders) {
        this.smsProviders = smsProviders;
    }


    public Counts getCounts() {
        return counts;
    }

    public void setCounts(Counts counts) {
        this.counts = counts;
    }


    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getActivationUrl() {
        return activationUrl;
    }

    public void setActivationUrl(String activationUrl) {
        this.activationUrl = activationUrl;
    }

    public int getJobsPageSize() {
        return counts.getJobsPageSize();
    }

    public void setJobsPageSize(int jobsPageSize) {
        this.counts.setJobsPageSize(jobsPageSize);
    }

    public int getCandidatesPageSize() {
        return counts.getCandidatesPageSize();
    }

    public void setCandidatesPageSize(int candidatesPageSize) {
        this.counts.setCandidatesPageSize(candidatesPageSize);
    }

    public String getUploadFolder() {
        return uploadFolder;
    }

    public void setUploadFolder(String uploadFolder) {
        this.uploadFolder = uploadFolder;
    }

    public String[] getOrigins() {
        return origins;
    }

    public void setOrigins(String[] origins) {
        this.origins = origins;
    }

    @Override
    public String toString() {
        return "EmployeeazConfiguration{" +
                "baseUrl='" + baseUrl + '\'' +
                ", activationUrl='" + activationUrl + '\'' +
                ", counts=" + counts +
                ", uploadFolder='" + uploadFolder + '\'' +
                ", origins=" + Arrays.toString(origins) +
                ", smsProviders=" + smsProviders +
                ", smsProviderMap=" + smsProviderMap +
                '}';
    }
}
