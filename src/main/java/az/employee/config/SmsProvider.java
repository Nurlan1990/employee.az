package az.employee.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SmsProvider implements Serializable {
    private static final long serialVersionUID = -4622670115098467307L;
    private String url;
    private String login;
    private String password;
    private List<String> senderNames;

    public SmsProvider() {
        this.url = "";
        this.login = "";
        this.password = "";
        this.senderNames = new ArrayList<>();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getSenderNames() {
        return senderNames;
    }

    public void setSenderNames(List<String> senderNames) {
        this.senderNames = senderNames;
    }

    @Override
    public String toString() {
        return "SmsProvider{" +
                "url='" + url + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", senderNames=" + senderNames +
                '}';
    }
}
