package az.employee.config;

import javax.sql.rowset.serial.SerialArray;
import java.io.Serializable;

public class Counts implements Serializable {
    private static final long serialVersionUID = -5128995442240471939L;
    private int jobsPageSize;
    private int candidatesPageSize;

    public int getJobsPageSize() {
        return jobsPageSize;
    }

    public void setJobsPageSize(int jobsPageSize) {
        this.jobsPageSize = jobsPageSize;
    }

    public int getCandidatesPageSize() {
        return candidatesPageSize;
    }

    public void setCandidatesPageSize(int candidatesPageSize) {
        this.candidatesPageSize = candidatesPageSize;
    }

    @Override
    public String toString() {
        return "Counts{" +
                "jobsPageSize=" + jobsPageSize +
                ", candidatesPageSize=" + candidatesPageSize +
                '}';
    }
}
