package az.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableScheduling
@EnableSwagger2
@SpringBootApplication
public class EmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeApplication.class, args);
	}


	@Bean
	public Docket getSwaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("az.employee"))
				.paths(PathSelectors.ant("/rest/**"))
				.build()
				.apiInfo(getApiInfo());
	}

	private ApiInfo getApiInfo() {
		return new ApiInfo(
				"Employee.az REST API Documentation",
				"Detailed documentation of REST api endpoints and models.",
				"1.0",
				"http://www.employee.az/terms",
				new Contact("Ramin Orujov", "http://linkedin.com/in/raminorujov", "raminorujov@gmail.com"),
				"Apache 2.0",
				"http://www.employee.az/license",
				Collections.emptyList()
		);
	}
}
