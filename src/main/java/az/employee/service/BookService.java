package az.employee.service;

import az.employee.entity.Author;
import az.employee.entity.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {

    List<Book> getBookList();
    Optional<Book> getBookById(long id);
    Book addBook(Book book);

    List<Author> getBookAuthors(long bookId);

//    Book updateBook(Book book);
//    void deleteBook(long bookId);
}
