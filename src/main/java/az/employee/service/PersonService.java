package az.employee.service;

import az.employee.entity.Person;

public interface PersonService {

    Person addPerson(Person person);
}
