package az.employee.service;

import az.employee.domain.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface CompanyService {
    Optional<Company> getCompanyByUserId(long userId);
    long getCompanyJobCount(long id);
    long getPageCount(long id);
    List<Job> getCompanyJobListByPage(long page,long companyId);
    void insertJob(Job job, HttpSession session);
    HashMap getCountryMap();
    HashMap getJobTypeMap();
    HashMap getJobCategoryMap();
    HashMap getCityMap();
    DataTableResult getCompanyJobDataTableResult(DataTableRequest dataTableRequest,HttpSession session);
    Job getJobByCompanyIdAndJobId(long jobId,long companyId);
    void editVacancyByJob(Job job);
    HashMap getIndustryMap();
    Company getCompanyByCompanyId(long companyId);
    void updateCompany(Company company);
    void deleteJobByIdAndCompanyId(long jobId , HttpSession session);
    DataTableResult dataResult(DataTableRequest request, HttpSession session, int start, int length);
    void deleteCandidateAllApps(long userId);
    List<JobUser> getCandidateNJobList(int page, long userId);
    long getCandidateNJobPageCount(long userId);
    long getIdFromJobUser(long jobId, long userId);
    void deleteCandidateApp(long id);
    long appliedJobCount(long userId);
}
