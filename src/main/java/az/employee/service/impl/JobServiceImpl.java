package az.employee.service.impl;

import az.employee.config.EmployeeazConfiguration;
import az.employee.domain.Job;
import az.employee.domain.JobCategoryCount;
import az.employee.domain.PageRequest;
import az.employee.domain.PageResponse;
import az.employee.repository.*;
import az.employee.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class JobServiceImpl implements JobService {

    private JobRepository jobRepository;

    @Autowired
    private EmployeeazConfiguration configuration;

//    @Value("${jobs.page.length}")
//    private long length;

    public JobServiceImpl(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }


    @Override
    public PageResponse<Job> getJobList(PageRequest request) {
        PageResponse<Job> pageResponse = new PageResponse<>();
        if (request.getPage() <= 0) {
            request.setPage(1);
        }

        if (request.getSize() <= 0) {
            request.setSize(configuration.getJobsPageSize());
        }

        pageResponse.setCurrentPage(request.getPage());
        pageResponse.setTotalCount(getAllJobCount());
        pageResponse.setPageCount(getPageCount(pageResponse.getTotalCount(), request.getSize()));
        // todo implement filtering
        pageResponse.setFilteredCount(pageResponse.getTotalCount());
        List<Job> jobs = getAllJobList(request.getPage(), request.getSize());
        System.out.println("jobs =" + jobs);
        pageResponse.setData(jobs);
        pageResponse.setDataCount(jobs.size());

        return pageResponse;
    }

    @Override
    public long getAllJobCount() {
        return jobRepository.getAllJobCount();
    }

    @Override
    public List<JobCategoryCount> getJobCategoryCountList() {
        return jobRepository.getJobCategoryCountList();
    }

    @Override
    public List<Job> getRecentJobs() {
        return jobRepository.getRecentJobs();
    }

    @Override
    public long getJobCategoryCountById(long id) {
        return jobRepository.getJobCategoryCountById(id);
    }

    @Override
    public List<Job> getJobListByCategoryId(long id, long page) {

        long offset = 0;
        if (page <= 1) {
            offset = 0;
        } else {
            offset = (page - 1) * configuration.getJobsPageSize();
        }
        // todo fix 5
        return jobRepository.getJobListByCategoryId(id, offset, 5);
    }


    @Override
    public List<Job> getAllJobList(long page, long size) {
        System.out.println(size);
        long offset = 0;
        if (page < 1) {
            offset = 0;
        } else {
            offset = (page - 1) * size;
        }

        return jobRepository.getAllJobList(offset, size);
    }

    @Override
    public long getPageCount(long count, long size) {

        long pageCount = count / size;

        if (count % size != 0) {
            pageCount++;
        }
        return pageCount;
    }

    @Override
    public Optional<Job> getJobById(long id) {
        return jobRepository.getJobById(id);
    }

    @Transactional
    @Override
    public Job addJob(Job job) {
        return jobRepository.addJob(job);
    }

    @Transactional
    @Override
    public Job updateJob(Job job) {
        return jobRepository.updateJob(job);
    }

    @Transactional
    @Override
    public boolean deleteJobById(long jobId) {
        return jobRepository.deleteJob(jobId);
    }
}
