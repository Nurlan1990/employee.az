package az.employee.service.impl;

import az.employee.entity.Address;
import az.employee.entity.Person;
import az.employee.repository.jpa.AddressRepository;
import az.employee.repository.jpa.ContactInfoRepository;
import az.employee.repository.jpa.PersonRepository;
import az.employee.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ContactInfoRepository contactInfoRepository;

    @Transactional
    @Override
    public Person addPerson(Person person) {
//        Address address = addressRepository.save(person.getAddress());
//        person.setAddress(address);

        System.out.println("before save person = " + person);
        person = personRepository.save(person);
        System.out.println("after save person = " + person);

        Person finalPerson = person;
        person.getContactInfoList().forEach(contactInfo -> {
            contactInfo.setPerson(finalPerson);
            contactInfoRepository.save(contactInfo);
        });

        return person;

    }
}
