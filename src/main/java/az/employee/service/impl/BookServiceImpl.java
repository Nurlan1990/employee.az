package az.employee.service.impl;

import az.employee.entity.Author;
import az.employee.entity.Book;
import az.employee.repository.jpa.AuthorRepository;
import az.employee.repository.jpa.BookRepository;
import az.employee.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public List<Book> getBookList() {
        return bookRepository.findAll();
    }

    @Override
    public Optional<Book> getBookById(long id) {
        return bookRepository.findById(id);
    }

    @Override
    public List<Author> getBookAuthors(long bookId) {
        return authorRepository.findAuthorsByBookId(bookId);
    }

    @Transactional
    @Override
    public Book addBook(Book book) {
        System.out.println("before save book = " + book);

        Book bookFromDb = bookRepository.save(book);
        System.out.println("book from db = " + bookFromDb);

        bookFromDb.getAuthors().forEach(author -> {
            author.getBooks().add(bookFromDb);
            authorRepository.save(author);
        });

        return bookFromDb;
    }
}
