package az.employee.service.impl;

import az.employee.domain.Token;
import az.employee.domain.User;
import az.employee.repository.TokenRepository;
import az.employee.service.TokenService;
import az.employee.util.CommonUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public Token generateToken(User user) {
        Token token = new Token();
        token.setValue(CommonUtility.generateToken());
        token.setGenerationDate(LocalDateTime.now());
        // todo refactor to configuration
        token.setExpireDate(LocalDateTime.now().plusDays(3));
        token.setUser(user);
        return token;
    }

    @Override
    public Token addToken(Token token) {
        return tokenRepository.addToken(token);
    }

    @Override
    public Optional<Token> getToken(String token) {
        return tokenRepository.getToken(token);
    }

    @Override
    public void markTokenAsUsed(String token) {
//        tokenRepository.updateToken()
    }
}
