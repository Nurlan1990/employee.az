package az.employee.service.impl;

import az.employee.config.EmployeeazConfiguration;
import az.employee.domain.Email;
import az.employee.domain.Token;
import az.employee.repository.EmailRepository;
import az.employee.service.EmailService;
import az.employee.util.EmailUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    private EmployeeazConfiguration configuration;

    @Autowired
    private JavaMailSender mailSender;

//    @Value("${employee.activation.url}")
//    private String activationUrl;

    @Override
    public Email generateActivationEmail(Token token) {
        Email email = new Email();
        email.setSubject(EmailUtility.registrationSubject());
        // todo move to config
        String link = configuration.getActivationUrl() + "?token=" + token.getValue();
        String body = EmailUtility.registrationEmail(
                token.getUser().getName(),
                token.getUser().getSurname(),
                link);
        email.setBody(body);
        System.out.println("activation email = " + email);
        return email;
    }

    @Override
    public Email addEmail(Email email) {
        System.out.println("add email " + email);
        return emailRepository.addEmail(email);
    }

    @Override
    public boolean isDuplicate(String email) {
        return emailRepository.isDuplicate(email);
    }

    @Async
    @Override
    public void sendEmail(String from, String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }
}
