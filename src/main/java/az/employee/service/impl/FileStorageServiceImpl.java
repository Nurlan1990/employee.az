package az.employee.service.impl;

import az.employee.config.EmployeeazConfiguration;
import az.employee.service.FileStorageService;
import az.employee.util.FileUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    @Autowired
    private EmployeeazConfiguration configuration;

//    @Value("${upload.folder}")
//    private String uploadFolder;

    @Override
    public String saveFile(long candidateId, MultipartFile file) {

        String filePart2 = "";

        try {
            //  /home/student/employeeaz2-upload    /11/20191211192645.png

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

            String filePart1 = configuration.getUploadFolder() + File.separator;
            filePart2 = String.format("%s%s%s%s",
                    candidateId,
                    File.separator,
                    LocalDateTime.now().format(formatter),
                    FileUtility.getFileExtension(file.getOriginalFilename())
            );

            String fileLocation = filePart1 + filePart2;

            System.out.println("file location = " + fileLocation);
            Path filePath = Paths.get(fileLocation);

            if (Files.notExists(filePath.getParent())) {
                System.out.println("parent dir does not exists, create new dir");
                Files.createDirectory(filePath.getParent());
            }

            Files.copy(file.getInputStream(), filePath);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return filePart2;
    }

    @Override
    public Resource getFile(String filename) {
        try {
            String fileLocation = configuration.getUploadFolder() + File.separator + filename;
            return new FileUrlResource(fileLocation);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error getting file " + filename);
        }
    }
}
