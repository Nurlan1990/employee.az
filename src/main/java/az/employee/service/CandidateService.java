package az.employee.service;

import az.employee.domain.*;

import java.util.List;
import java.util.Optional;

public interface CandidateService {

    Candidate register(Candidate candidate);

    DataTableResult getDataTableResponse(DataTableRequest dataTableRequest);
    List<Candidate> getCandidateList(int page);
    long getCandidatePageCount();

    Optional<Candidate> getCandidateById(long id);

    List<JobHistory> getJobHistoryList(long candidateId);
    JobHistory addJobHistory(JobHistory jobHistory);
    Optional<JobHistory> getJobHistoryById(long id);
    JobHistory updateJobHistory(JobHistory jobHistory);
    void deleteJobHistory(long id);

    void saveProfileImage(Candidate candidate);
}
