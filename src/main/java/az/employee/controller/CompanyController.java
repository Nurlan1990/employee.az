package az.employee.controller;

import az.employee.domain.*;
import az.employee.service.CandidateService;
import az.employee.service.CompanyService;
import az.employee.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RequestMapping("/company/")
@Controller
public class CompanyController {

    @Autowired
    private CompanyService companyService;
    @Autowired
    private JobService jobService;
    @Autowired
    private CandidateService candidateService;

    @GetMapping("/")
    public String index(HttpSession session) {
        User user = (User) session.getAttribute("user");
        System.out.println(user.getId() + " geldi !");
        long appliedJobCount = companyService.appliedJobCount(user.getId());
        session.setAttribute("appliedJobCount",appliedJobCount);
        Optional<Company> optionalCompany = companyService.getCompanyByUserId(user.getId());
        if (optionalCompany.isPresent()) {
            Company company = optionalCompany.get();
            session.setAttribute("company", company);
            System.out.println(company);
        } else {
            throw new RuntimeException("Bu usere aid sirket tapilmadi " + user.getId());
        }
        return "company/index";
    }

    @GetMapping("/company-profile")
    public ModelAndView companyProfile(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("company/company");
        if (session.getAttribute("company") != null) {
            Company company = (Company) session.getAttribute("company");
            Long jobCount = companyService.getCompanyJobCount(company.getId());
            modelAndView.addObject("company", company);
            modelAndView.addObject("jobCount", jobCount);
        }
        return modelAndView;
    }

    @GetMapping("/viewJobs")
    public ModelAndView test(@RequestParam(name = "page", defaultValue = "1") long page, HttpSession session) {
        Company company = (Company) session.getAttribute("company");
        System.out.println("Company = " + company);
        System.out.println(" - ---- - -- - -- - ");
        long pageCount = companyService.getPageCount(company.getId());
        System.out.println("pageCount " + pageCount);
        System.out.println("currentPage " + page);
        ModelAndView modelAndView = new ModelAndView("company/jobs");
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("pageSize", pageCount);
        List<Job> jobList = companyService.getCompanyJobListByPage(page, company.getId());
        modelAndView.addObject("jobList", jobList);
        System.out.println(jobList);
        return modelAndView;
    }

    @GetMapping("/jobdetail")
    public ModelAndView jobdetail(@RequestParam(name = "jobId") long jobId, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("company/job_detail");
        Company company = (Company) session.getAttribute("company");
        Job job = companyService.getJobByCompanyIdAndJobId(jobId, company.getId());
        modelAndView.addObject("job", job);
        return modelAndView;
    }

    @GetMapping("/addVacancy")
    public ModelAndView test2() {
        ModelAndView modelAndView = new ModelAndView("company/addvacancy");
        Job job = new Job();
        HashMap countryHashMap = companyService.getCountryMap();
        HashMap jobTypeHashMap = companyService.getJobTypeMap();
        HashMap jobCategoryMap = companyService.getJobCategoryMap();
        HashMap cityMap = companyService.getCityMap();

        System.out.println(countryHashMap);
        System.out.println(jobTypeHashMap);
        System.out.println(jobCategoryMap);
        System.out.println(cityMap);
        modelAndView.addObject("countryMap", countryHashMap);
        modelAndView.addObject("jobTypeHashMap", jobTypeHashMap);
        modelAndView.addObject("jobCategoryMap", jobCategoryMap);
        modelAndView.addObject("cityMap", cityMap);
        modelAndView.addObject("job", job);
        return modelAndView;


    }

    @PostMapping("/addVacancy")
    public ModelAndView modelAndView(@ModelAttribute(name = "job") Job job, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            companyService.insertJob(job, session);
            modelAndView.setViewName("company/index");
        } catch (Exception e) {
            modelAndView.setViewName("web/error");
        }

        return modelAndView;
    }

    @GetMapping("/editvacancy")
    public ModelAndView editVacancy(@RequestParam(name = "jobId") long jobId,
                                    HttpSession session) {

        Company company = (Company) session.getAttribute("company");
        Job job = companyService.getJobByCompanyIdAndJobId(jobId, company.getId());
        HashMap countryMap = companyService.getCountryMap();
        HashMap cityMap = companyService.getCityMap();
        HashMap jobCategoryMap = companyService.getJobCategoryMap();
        HashMap jobTypeMap = companyService.getJobTypeMap();


        ModelAndView modelAndView = new ModelAndView("company/editvacancy");
        modelAndView.addObject("job", job);
        modelAndView.addObject("countryMap", countryMap);
        modelAndView.addObject("cityMap", cityMap);
        modelAndView.addObject("jobCategoryMap", jobCategoryMap);
        modelAndView.addObject("jobTypeMap", jobTypeMap);
        return modelAndView;
    }


    @PostMapping("/editvacancy")
    public ModelAndView editvacancy(@ModelAttribute(name = "job") Job job) {
        System.out.println("Post Request Geldi !");
        ModelAndView modelAndView = new ModelAndView("company/index");
        System.out.println(job);
        System.out.println("Address Id " + job.getAddress().getId());
        try {
            companyService.editVacancyByJob(job);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        return modelAndView;
    }

    @GetMapping("/getJobsByAjax")
    @ResponseBody
    public DataTableResult getJobsByAjax(@RequestParam(name = "start") int start,
                                         @RequestParam(name = "length") int length,
                                         @RequestParam(name = "search[value]") String searchValue,
                                         @RequestParam(name = "order[0][column]") int column,
                                         @RequestParam(name = "order[0][dir]") String orderDir,
                                         @RequestParam(name = "draw") int draw,
                                         HttpSession session) {

        DataTableRequest dataTableRequest = new DataTableRequest();
        dataTableRequest.setDraw(draw);
        dataTableRequest.setLength(length);
        dataTableRequest.setStart(start);
        dataTableRequest.setSortColumn(column);
        dataTableRequest.setSortDirection(orderDir);
        dataTableRequest.setFilter(searchValue);

        DataTableResult dataTableResult = new DataTableResult();
        return companyService.getCompanyJobDataTableResult(dataTableRequest, session);
    }

    @GetMapping("/editcompany")
    public ModelAndView editcompany(HttpSession session) {
        Company company = (Company) session.getAttribute("company");
        HashMap industryMap = companyService.getIndustryMap();
        System.out.println(industryMap);
        //todo daha yaxsi yolu
        List<Boolean> list = new ArrayList<>();
        list.add(true);
        list.add(false);
        ModelAndView modelAndView = new ModelAndView("company/editcompany");
        modelAndView.addObject("industryMap", industryMap);
        modelAndView.addObject("company", company);
        modelAndView.addObject("listBoolean", list);


        return modelAndView;
    }

    @PostMapping("/editcompany")
    public ModelAndView editCompany(@ModelAttribute(name = "company") Company company) {

        ModelAndView modelAndView = new ModelAndView();
        try {
            companyService.updateCompany(company);
            modelAndView.setViewName("company/index");
        } catch (Exception e) {
            modelAndView.setViewName("company/error");
            System.out.println("Sizin Error : ");
            System.out.println(e.getMessage());
        }
        return modelAndView;
    }

    @GetMapping("/deletevacancy")
    public String deletevacancy(@RequestParam(name = "jobId") long jobId, HttpSession session) {
        try {
            companyService.deleteJobByIdAndCompanyId(jobId, session);
            return "redirect:/company/";
        } catch (Exception e) {
            return "company/error";
        }
    }

    @GetMapping("/contact")
    public ModelAndView contact() {
        ModelAndView modelAndView = new ModelAndView("company/contact");
        return modelAndView;
    }


    @GetMapping("/getCanAppJobListByAjax")
    @ResponseBody
    public DataTableResult getCanAppJobListByAjax(
            @RequestParam(name = "start") int start,
            @RequestParam(name = "length") int length,
            @RequestParam(name = "search[value]") String searchValue,
            @RequestParam(name = "order[0][column]") int column,
            @RequestParam(name = "order[0][dir]") String sorDirection,
            @RequestParam(name = "draw") int draw,
            HttpSession session){

        DataTableRequest dataTableRequest =new DataTableRequest();


        dataTableRequest.setDraw(draw);
        dataTableRequest.setLength(length);
        dataTableRequest.setStart(start);
        dataTableRequest.setSortColumn(column);
        dataTableRequest.setSortDirection(sorDirection);
        dataTableRequest.setFilter(searchValue);

        return companyService.dataResult(dataTableRequest, session, start, length);
    }

    @GetMapping("/viewProfile")
    public ModelAndView viewProfile(
            @RequestParam(name = "candidateId") long id
    ) {
        ModelAndView modelAndView = new ModelAndView("web/candidate");


        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(id);

        if (optionalCandidate.isPresent()) {
            modelAndView.addObject("candidate", optionalCandidate.get());
        }
        return modelAndView;
    }

    @GetMapping("/deleteCandidate")
    public ModelAndView deleteCandidateAllApps(
            @RequestParam(name = "userId") long userId
    ) {
        ModelAndView modelAndView = new ModelAndView("company/contact");

        companyService.deleteCandidateAllApps(userId);

        return modelAndView;
    }

    @GetMapping("/clients")
    public ModelAndView clients(
            @RequestParam(name = "page", defaultValue = "1") int page,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView("company/clients");

        User user = (User) session.getAttribute("user");
        List<JobUser> candidateNJobList = companyService.getCandidateNJobList(page,user.getId());
        long pageCount = companyService.getCandidateNJobPageCount(user.getId());


        modelAndView.addObject("candidateNJobList", candidateNJobList);
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("pageCount", pageCount);

        return modelAndView;
    }

    @GetMapping("/viewJob")
    public ModelAndView viewJob(
            @RequestParam(name = "jobId") long id
    ) {
        ModelAndView modelAndView = new ModelAndView("web/job_detail");
        Optional<Job> optionalJob = jobService.getJobById(id);
        if (optionalJob.isPresent()) {
            modelAndView.addObject("job", optionalJob.get());
        }
        return modelAndView;
    }


    @GetMapping("/deleteCandidateApp")
    public ModelAndView deleteCandidateApp(
            @RequestParam(name = "jobId") long jobId,
            @RequestParam(name = "userId") long userId,
            @RequestParam(name = "page", defaultValue = "1") int page,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView("company/clients");
        long id= companyService.getIdFromJobUser(jobId, userId);
        companyService.deleteCandidateApp(id);

        User user = (User) session.getAttribute("user");
        List<JobUser> candidateNJobList = companyService.getCandidateNJobList(page,user.getId());
        long pageCount = companyService.getCandidateNJobPageCount(user.getId());

        modelAndView.addObject("candidateNJobList", candidateNJobList);
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("pageCount", pageCount);

        return modelAndView;
    }


}
