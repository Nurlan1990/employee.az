package az.employee.controller;

import az.employee.config.EmployeeazConfiguration;
import az.employee.domain.*;
import az.employee.service.*;
import az.employee.util.FormUtil;
import az.employee.validator.CandidateRegistrationFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static az.employee.domain.Role.CANDIDATE;
import static az.employee.domain.Role.COMPANY;
import static az.employee.util.SecurityUtility.hasRole;

@RequestMapping("/")
@Controller
public class WebController {

    @Autowired
    private CandidateRegistrationFormValidator candidateRegistrationFormValidator;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private JobService jobService;

    @Autowired
    private EmployeeazConfiguration configuration;

    @InitBinder
    public void dataBind(WebDataBinder dataBinder) {
        if (dataBinder.getTarget() != null
                && dataBinder.getTarget().getClass().equals(CandidateRegistrationForm.class)) {
            dataBinder.setValidator(candidateRegistrationFormValidator);
        }
    }

    @GetMapping("/")
    public ModelAndView index() {

        System.out.println("configuration = " + configuration);

        ModelAndView modelAndView = new ModelAndView("web/index");
        modelAndView.addObject("jobCount", jobService.getAllJobCount());
        modelAndView.addObject("jobCategoryCountList", jobService.getJobCategoryCountList());
        modelAndView.addObject("recentJobs", jobService.getRecentJobs());
        return modelAndView;
    }

    @GetMapping("/register")
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("web/register");
        CandidateRegistrationForm form = new CandidateRegistrationForm();
        modelAndView.addObject("candidateRegistrationForm", form);
        return modelAndView;
    }

    @PostMapping("/register_candidate")
    public ModelAndView registerCandidate(@ModelAttribute("candidateRegistrationForm") @Validated CandidateRegistrationForm form,
                                          BindingResult validationResult) {
        ModelAndView modelAndView = new ModelAndView();

        System.out.println("form = " + form);

        if (validationResult.hasErrors()) {
            System.out.println("form is invalid");
            System.out.println("error count = " + validationResult.getErrorCount());
            System.out.println("errors = " + validationResult.getAllErrors());
            modelAndView.setViewName("web/register");
        } else {
            System.out.println("form is valid");
            try {
                Candidate candidate = FormUtil.getCandidate(form);
                candidate = candidateService.register(candidate);
                modelAndView.setViewName("web/register-success");
            } catch (Exception e) {
                e.printStackTrace();
                modelAndView.setViewName("web/register-error");
            }
        }

        return modelAndView;
    }

    @GetMapping("/login")
    public String login() {
        System.out.println("Login Get Geldi !");
        return "web/login";
    }

    /*
    @PostMapping("/login")
    public ModelAndView checkLogin(
            @RequestParam(name = "email") String email,
            @RequestParam(name = "password") String password,
            HttpSession session
    ) {
        System.out.println("Request Post Geldi !!! ");
        System.out.println(email);
        System.out.println(password);
        ModelAndView modelAndView = new ModelAndView();

        Optional<User> optionalUser = userService.getUserByEmail(email);
        String errorMessage = "";

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            System.out.println(user);
            System.out.println(password);
            System.out.println(user.getPassword());
            if (passwordService.checkPassword(password, user.getPassword())) {
                System.out.println("Password Service Isledi !");
                // parol dogrudur
                // status = PENDING, aktivlesdir mesaji goster
                if (user.getStatus() == UserStatus.PENDING) {
                    errorMessage = "Profile is not active";
                } else if (user.getStatus() == UserStatus.ACTIVE) {
                    System.out.println("Active Isledi !");
                    System.out.println(user.getName());
                    // status = ACTIVE, rollarini gotur, ona gore lazimi sehifeye redirect et
                    user.setRoleList(userService.getUserRoles(user.getId()));
                    session.setAttribute("user", user);
                    session.setAttribute("loginTime", LocalDateTime.now());

                    String page = "/";
                    if (hasRole(user, Role.ADMIN)) {
                        page = "/admin/";
                    } else if (hasRole(user, COMPANY)) {
                        page = "/company/";
                    } else if (hasRole(user, CANDIDATE)) {
                        page = "/candidate/";
                    }

                    modelAndView.setViewName("redirect:" + page);
                    System.out.println(modelAndView.getViewName());
                } else {
                    // status = DELETED/LOCKED
                    errorMessage = "Login not allowed";
                }

            } else {
                // parol yanlisdir
                errorMessage = "Invalid username or password";
                System.out.println("Invalid username or password");
            }
        } else {
            errorMessage = "Invalid username or password";
        }

        if (!errorMessage.isEmpty()) {
            modelAndView.setViewName("web/login");
            modelAndView.addObject("errorMessage", errorMessage);
        }

       System.out.println(modelAndView.getViewName());
        return modelAndView;
    }

     */

    @GetMapping("/activate")
    public ModelAndView activate(@RequestParam(name = "token") String token) {
        ModelAndView modelAndView = new ModelAndView();

        /*
         * 1.get token param from request
         * 2.validate token
         *   2.1.if token not empty/null
         *       1.get token from db
         *           1.if token is not expired and token not used
         *               1.check user status, if user status = PENDING
         *                   activate user status to ACTIVE
         *                   mark token as used
         *               2.else if user status = ACTIVE, DELETED, LOCKED
         *                   status = ACTIVE, mark token as used
         *                   status = DELETED, mark token as used, log activity
         *                   status = LOCKED, mark token as used, log activity
         *           2.if token expired or token is used
         *               show msg, token expired and resend token link
         *       2.token not exists in db
         *           error invalid token, or redirect to main page
         *   2.2 if token is empty/null
         *      error invalid token, or redirect to main page
         *
         * */

        String errorMessage = "";

        // 1.get token param from request
        if (token != null && !token.isEmpty()) {
            // 2.1.if token not empty/null
            //  2.1.1 get token from db
            Optional<Token> optionalToken = tokenService.getToken(token);

            if (optionalToken.isPresent()) {
                Token tokenObj = optionalToken.get();
                /*
                 * 1.if token is not expired and token not used
                 *               1.check user status, if user status = PENDING
                 *                   activate user status to ACTIVE
                 *                   mark token as used
                 *               2.else if user status = ACTIVE, DELETED, LOCKED
                 *                   status = ACTIVE, mark token as used
                 *                   status = DELETED, mark token as used, log activity
                 *                   status = LOCKED, mark token as used, log activity
                 *           2.if token expired or token is used
                 *               show msg, token expired and resend token link
                 * */
                // 1.if token is not expired and token not used
                if (tokenObj.getExpireDate().isAfter(LocalDateTime.now())
                        && !tokenObj.isUsed()) {

//                    Optional<User> optionalUser = userService.getUserById(tokenObj.getUserId());
                    Optional<User> optionalUser = Optional.empty();
                    if (optionalUser.isPresent()) {
                        User user = optionalUser.get();
                        // 1.check user status, if user status = PENDING
                        //                 *                   activate user status to ACTIVE
                        //                 *                   mark token as used
                        //

                        if (user.getStatus() == UserStatus.PENDING) {
                            //  todo activate user status to ACTIVE
//                            userService.activateUser(user.getId());

                            //  todo mark token as used
                            tokenService.markTokenAsUsed(token);

                            modelAndView.setViewName("web/activation-success");
                        } else {
                            // mark token as used
                            tokenService.markTokenAsUsed(token);
                            // log activity

                            //    2.else if user status = ACTIVE, DELETED, LOCKED
                            //           status = ACTIVE, mark token as used
                            //            status = DELETED, mark token as used, log activity
                            //          status = LOCKED, mark token as used, log activity
                            modelAndView.setViewName("web/activation-error");
                        }
                    } else {
                        // user not found by id, inconsistency between token and user tables
                        // todo send email to admin
                        System.out.println("user not found by id, inconsistency between token and user tables");
                        modelAndView.setViewName("web/activation-error");
                    }
                } else {
                    //  2.if token expired or token is used
                    //  show msg, token expired and resend token link
                    errorMessage = "Token is expired, <a href='/web?action=resend_activation_token&user_id=" + tokenObj.getUser().getId() + "'>Resend?</a>";
                }

            } else {
                // 2.token not exists in db
                //   error invalid token, or redirect to main page
                errorMessage = "Invalid token";
            }

        } else {
            // 2.2 if token is empty/null
            // error invalid token, or redirect to main page
            errorMessage = "Yanlis token";
        }

        if (!errorMessage.isEmpty()) {
            modelAndView.addObject("errorMessage", errorMessage);
            modelAndView.setViewName("web/token-error");
        }

        return modelAndView;
    }

    @GetMapping("/checkEmail")
    @ResponseBody
    public CheckEmailResponse checkEmail(@RequestParam(name = "email") String email) {
        boolean result = emailService.isDuplicate(email);
        CheckEmailResponse response = new CheckEmailResponse();
        response.setEmail(email);
        response.setDuplicate(result);
        if (result) {
            response.setMessage("Email already registered");
        } else {
            response.setMessage("Email not registered");
        }

        return response;
    }

    @GetMapping("/checkEmail2")
    public ResponseEntity<CheckEmailResponse> checkEmail2(@RequestParam(name = "email") String email) {
        boolean result = emailService.isDuplicate(email);
        CheckEmailResponse response = new CheckEmailResponse();
        response.setEmail(email);
        response.setDuplicate(result);
        if (result) {
            response.setMessage("Email already registered");
        } else {
            response.setMessage("Email not registered");
        }

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.add("itcity", "academy");

        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "web/access-denied";
    }

    @GetMapping("/jobs")
    public ModelAndView jobs(
            @RequestParam(name = "category", required = false, defaultValue = "0") Long categoryId,
            @RequestParam(name = "page", defaultValue = "1") long page,
            @RequestParam(name = "size", defaultValue = "6") long size
    ) {

        System.out.println("categoryId = " + categoryId);
        ModelAndView modelAndView = new ModelAndView("web/jobs");

        modelAndView.addObject("jobCategoryCountList", jobService.getJobCategoryCountList());

        long count = 0;
        List<Job> jobList = null;
        if (categoryId == 0) {
            count = jobService.getAllJobCount();
            jobList = jobService.getAllJobList(page, size);
        } else {
            count = jobService.getJobCategoryCountById(categoryId);
            jobList = jobService.getJobListByCategoryId(categoryId, page);
        }
        long pageSize = 0;
        if (count > 0) {
            pageSize = jobService.getPageCount(count, size);
        }

        System.out.println("current Page " + page);
        System.out.println("pageSize " + pageSize);
        System.out.println("Count " + count);
        modelAndView.addObject("categoryID", categoryId);
        modelAndView.addObject("jobList", jobList);
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.addObject("count", count);
        return modelAndView;
    }

    @GetMapping("/jobs-ajax")
    public String getJobsAjax() {
        return "web/jobs-ajax";
    }

    @GetMapping("/candidates")
    public ModelAndView candidates(
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        ModelAndView modelAndView = new ModelAndView("web/candidates");

        List<Candidate> candidateList = candidateService.getCandidateList(page);
        long pageCount = candidateService.getCandidatePageCount();
        modelAndView.addObject("candidateList", candidateList);
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("pageCount", pageCount);
        return modelAndView;
    }

    @GetMapping("/candidate")
    public ModelAndView viewCandidate(
            @RequestParam(name = "id") long id
    ) {
        ModelAndView modelAndView = new ModelAndView("web/candidate");

        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(id);
        if (optionalCandidate.isPresent()) {
            modelAndView.addObject("candidate", optionalCandidate.get());
        }
        return modelAndView;
    }
}
