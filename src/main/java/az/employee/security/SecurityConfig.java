package az.employee.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private EmployeeUserDetailsService userDetailsService;

    @Autowired
    private EmployeeAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private EmployeeAuthenticationFailureHandler authenticationFailureHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .httpBasic()
                .and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/rest/jobs/*").permitAll()
                .antMatchers(HttpMethod.POST, "/rest/jobs/*").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/rest/jobs/*").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/rest/jobs/*").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .and().formLogin().disable()
                .csrf().disable();

        http.csrf().disable()
                .authorizeRequests()
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .antMatchers("/candidate/**").hasAnyRole("CANDIDATE", "ADMIN")
                    .antMatchers("/company/**").hasAnyRole("COMPANY", "ADMIN")
                    .antMatchers("/").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .permitAll();


    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("ali@gmail.com")
//                .password("123456")
//                .roles("ADMIN")
//                .and()
//                .withUser("tofiq@canada.com")
//                .password("canada")
//                .roles("CANDIDATE", "ADMIN")
//                .and()
//                .withUser("hr@azercell.com")
//                .password("azercell123")
//                .roles("COMPANY");

        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(getPasswordEncoder());
        return authenticationProvider;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
