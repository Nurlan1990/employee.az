package az.employee.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.*;

@Table(name = "address_table")
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String info;

    // relationship owner side - Person
//    @JsonIgnore
//    @OneToOne(mappedBy = "address")
//    private Person person;

    // // relationship owner side - Address
//    @OneToOne()
//    @JoinColumn(name = "person_id", referencedColumnName = "person_id")
//    private Person person;

    public Address() {
    }

//    public Person getPerson() {
//        return person;
//    }
//
//    public void setPerson(Person person) {
//        this.person = person;
//    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
