package az.employee.entity;

public interface NameCount {
    String getName();
    long getCount();
}
