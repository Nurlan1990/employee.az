package az.employee.constants;

public class ValidationConstants {

    public static final int FIRST_NAME_MIN_LENGTH = 2;
    public static final int FIRST_NAME_MAX_LENGTH = 50;
    public static final int EMAIL_MAX_LENGTH = 50;
}
