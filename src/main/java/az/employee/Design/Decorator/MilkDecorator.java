package az.employee.Design.Decorator;

import java.math.BigDecimal;

public class MilkDecorator extends CofeeDecorator {
    public MilkDecorator(Cofee cofee) {
        super(cofee);
    }

    @Override
    public String getIngridients() {
        return super.getIngridients()+" with milk";
    }

    @Override
    public BigDecimal getCost() {
        return super.getCost().add(BigDecimal.valueOf(1));
    }
}
