package az.employee.Design.Decorator;

public class DecoratorMain {
    public static void main(String[] args) {
          Cofee cofee1 = new Vannill(new Espresso());
          print(cofee1);
    }

    public static void print(Cofee cofee){
        System.out.println(" cofee name: " + cofee.getIngridients() + " " + " cofee cost " + cofee.getCost());
    }
}
