package az.employee.Design.Decorator;

import java.math.BigDecimal;

public class Espresso implements Cofee {

    @Override
    public String getIngridients() {
        return "esprresso";
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(6);
    }
}
