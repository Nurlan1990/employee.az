package az.employee.Design.Decorator;

import java.math.BigDecimal;

public interface Cofee {
    String getIngridients();
    BigDecimal getCost();
}
