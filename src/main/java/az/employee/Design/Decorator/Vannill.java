package az.employee.Design.Decorator;

import java.math.BigDecimal;

public class Vannill extends CofeeDecorator {
    public Vannill(Cofee cofee) {
        super(cofee);
    }


    @Override
    public String getIngridients() {
        return super.getIngridients() +  " with vanil";
    }

    @Override
    public BigDecimal getCost() {
        return super.getCost().add(BigDecimal.valueOf(2));
    }
}
