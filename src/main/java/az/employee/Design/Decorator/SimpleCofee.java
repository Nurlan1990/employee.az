package az.employee.Design.Decorator;

import java.math.BigDecimal;

public class SimpleCofee implements Cofee{
    @Override
    public String getIngridients() {
        return "simmple cofee";
    }

    @Override
    public BigDecimal getCost() {
        return BigDecimal.valueOf(5);
    }
}
