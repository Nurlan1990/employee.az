package az.employee.Design.Decorator;

import java.math.BigDecimal;

public abstract class CofeeDecorator implements Cofee{
    private Cofee cofee;

    public CofeeDecorator(Cofee cofee) {
        this.cofee = cofee;
    }


    @Override
    public String getIngridients() {
        return cofee.getIngridients();
    }

    @Override
    public BigDecimal getCost() {
        return cofee.getCost();
    }
}
