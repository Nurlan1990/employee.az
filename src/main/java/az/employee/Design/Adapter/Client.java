package az.employee.Design.Adapter;

public class Client {

    private HrSystem system;

    public Client(HrSystem system) {
        this.system = system;
    }

    public void run(){
        System.out.println("Client is using hr system");
        system.work();
    }
}
