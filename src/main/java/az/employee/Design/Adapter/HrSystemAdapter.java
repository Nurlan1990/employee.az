package az.employee.Design.Adapter;

public class HrSystemAdapter implements HrSystem{

    private AdvancedHrSystem newSystem;

    public HrSystemAdapter(AdvancedHrSystem newSystem) {
        this.newSystem = newSystem;
        System.out.println("new hr system adapter yarandi");
    }


    @Override
    public void work() {
        System.out.println("aadapter is working");
        newSystem.process1();
        newSystem.process2();

    }
}
